﻿using PicCreator.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PicCreator.ViewModel
{

    public class MainVM : Notifier
    {

        private Factory _factory ;

        public MainVM()
        {
            _factory = new Factory();
            _factory.onElementChanged += _factory_onElementChanged;
        }

        private void _factory_onElementChanged(object? sender, EventArgs e)
        {
            
            if(!CircuitTable.keys.Contains("Circuit"))
                CircuitTable.Add("Circuit","Circuit");
            
        }

        #region Properties

        public BindableDictionnary<string,string> CircuitTable { get; set; } = new BindableDictionnary<string,string>();

        public ObservableCollection<DataRow> dataTableCollection { get; set; } = new ObservableCollection<DataRow>();


        private bool _isPgVisible;
        public bool IsPgVisible
        {
            get => _isPgVisible;
            set => SetAndRaiseProperty(ref _isPgVisible, value);
        }


        private string _textCircuit;
        public string textCircuit
        {
            get => _textCircuit;
            set => SetAndRaiseProperty(ref _textCircuit, value);
        }

        private string _selectedCircuit;
        public string SelectedCircuit
        {
            get => _selectedCircuit;
            set
            {
                if (value != null)
                {
                    _factory._elementsByCircuit.TryGetValue(value, out var list);
                    dataTableCollection.Clear();
                    list?.ForEach(x => dataTableCollection.Add(x));
                }
                
                SetAndRaiseProperty(ref _selectedCircuit, value);
                if (value != null) AssociatedValue = CircuitTable[value];
                else AssociatedValue = string.Empty;
            }
        }

        private string _associatedValue;
        public string AssociatedValue
        {
            get => _associatedValue;
            set
            {
                
                SetAndRaiseProperty(ref _associatedValue, value);
                if (!string.IsNullOrEmpty(value)) CircuitTable.ModifyPair(SelectedCircuit, value);
                
            }
        }


        #endregion Properties


        #region Command

        private ICommand deleteItem;

        public ICommand DeleteItem => deleteItem ?? (deleteItem = new SimpleCommand(DeleteItemExecute,DeleteItenCanExecute));

        private void DeleteItemExecute(object obj)
        {
            
            _factory._elementsByCircuit.Remove(SelectedCircuit);
            CircuitTable.Remove(SelectedCircuit);
            _factory.BuildCircuitDictionnary(true);
        }

        private bool DeleteItenCanExecute(object arg)
        {
            return SelectedCircuit != null;
        }

        private ICommand _addCircuit;

        public ICommand AddCircuit => _addCircuit ?? (_addCircuit = new SimpleCommand(AddItemExecute, AddItenCanExecute));

        private void AddItemExecute(object obj)
        {
            CircuitTable.Add(textCircuit,textCircuit);
            _factory._elementsByCircuit.Add(textCircuit,null);
            textCircuit = string.Empty;
            _factory.BuildCircuitDictionnary(true);
        }

        private bool AddItenCanExecute(object arg)
        {
            return  !string.IsNullOrEmpty(textCircuit);
        }


        private ICommand _downLoad;

        public ICommand DownLoad => _downLoad ?? (_downLoad = new SimpleCommand(DownLoadExecute, DownLoadCanExecute));

        private void DownLoadExecute(object obj)
        {
            CircuitTable.Clear();
            _factory._elementsByCircuit.Clear();
            try
            {
                _factory.BuildCircuitDictionnary(false);
                _factory._elementsByCircuit.Keys.ToList().ForEach(x => CircuitTable.Add(x, x));
            }
            catch(Exception e)
            {
                XLSReader.GetInstance().con?.Close();
                MessageBox.Show($"Error : {e}","Warning");
            } 
        }

        private bool DownLoadCanExecute(object arg)
        {
            return true;
        }

        private ICommand _draw;

        public ICommand Draw => _draw ?? (_draw = new SimpleCommand(DrawExecute, DrawCanExecute));

        private bool _isPicAlreadyDraw;
        private void DrawExecute(object obj)
        {
            _factory._elementsByCircuit = CircuitTable.GenerateDictionnary(_factory._elementsByCircuit,"Accueil"); 
            IsPgVisible = true;
            _isPicAlreadyDraw=_factory.MakeImage();
            IsPgVisible = false;
            XLSReader.GetInstance().fileWrite.Close();
            MessageBox.Show("Ecriture terminé avec succés","Information");
        }

        private bool DrawCanExecute(object arg)
        {
            return CircuitTable != null && CircuitTable.keys.Count()> 0 && XLSReader.GetInstance().GetFileWrite().BaseStream.CanWrite && !_isPicAlreadyDraw;
        }

        #endregion Command


    }
}
