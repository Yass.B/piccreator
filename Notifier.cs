﻿

using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace PicCreator
{
    public class Notifier : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;
        public void OnPropertyRaised(string propertyname)
        {

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyname));

        }

        protected bool SetAndRaiseProperty<T>(ref T field, T newValue, [CallerMemberName] string propertyName = null)
        {
            if (!Equals(field, newValue))
            {
                field = newValue;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
                return true;
            }

            return false;
        }


    }
}
